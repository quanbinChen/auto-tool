# -*- coding:utf-8 -*-
# @Time   : 2021/9/9
# @Author : quanbin

import pymysql.cursors
from config import CONFIG

connection = pymysql.connect(host=CONFIG['db_host'],
                             user=CONFIG['db_user'],
                             password=CONFIG['db_password'],
                             database=CONFIG['db_name'],
                             cursorclass=pymysql.cursors.DictCursor
                             )


def insert_job(job_id, job_title):
    with connection.cursor() as cursor:
        sql = "INSERT INTO job_info (job_id, job_title) VALUES (%s,%s)"
        cursor.execute(sql, (job_id, job_title))
        connection.commit()


def fetch_all_jobs():
    with connection.cursor() as cursor:
        sql = "SELECT job_id FROM job_info"
        cursor.execute(sql)
        result = cursor.fetchall()
        print(result)
        return [a['job_id'] for a in result]


def insert_tpe(job_id, total_size):
    with connection.cursor() as cursor:
        sql = "INSERT INTO tpe (job_id, total_size, created_on) VALUES (%s,%s,NOW())"
        cursor.execute(sql, (job_id, total_size))
        connection.commit()


def get_job_last_10(job_id):
    with connection.cursor() as cursor:
        sql = "SELECT total_size from tpe WHERE job_id=%s ORDER BY created_on DESC LIMIT 11"
        cursor.execute(sql, job_id)
        result = cursor.fetchall()
        print(result)
        return [int(a["total_size"]) for a in result]


if __name__ == '__main__':
    # print(fetch_all_jobs())
    # insert_tpe(18234, 21394)
    print(get_job_last_10(18981))
