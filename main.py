# -*- coding:utf-8 -*-
# @Time   : 2021/9/9
# @Author : quanbin

from report import *
from api_helper import do_batch


if __name__ == '__main__':
    do_batch()
    result, fill_color, ret_code = data_stastic()
    gen_html_report(result, fill_color)
    if ret_code != 0:
        exit(1)
