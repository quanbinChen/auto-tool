# -*- coding:utf-8 -*-
# @Time   : 2021/9/9
# @Author : quanbin

import base64


def generate_user_token(username, password):
    """
    generate user token
    :param username:
    :param password:
    :return:
    """
    s = '{}:{}'.format(username, password)
    s1 = base64.b64encode(s.encode('utf-8')).decode("utf-8")
    return 'Basic {}'.format(s1)