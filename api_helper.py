# -*- coding:utf-8 -*-
# @Time   : 2021/9/9
# @Author : quanbin


import requests
from util import *
from db_helper import *

token = generate_user_token(CONFIG["account"], CONFIG["password"])


def get_talent_pool_size(job_id):
    url = 'https://test.celential.ai/api/jobs/{}/sourcing/talentPoolSize?onlyTotal=true'.format(job_id)
    headers = {"Authorization": token}
    r = requests.post(url, headers=headers)
    result = r.json()['total']
    print(result)
    return result


def import_jobs():
    url = 'https://test.celential.ai/api/jobDesc?fields=settings,schedule,ATS,reachoutSetting,skills&status=0'
    headers = {"Authorization": token}
    r = requests.get(url, headers=headers)
    result = r.json()
    for item in result:
        insert_job(item['id'], item['jobtitle'])


def do_batch():
    job_list = fetch_all_jobs()
    for job_id in job_list:
        result = get_talent_pool_size(job_id)
        insert_tpe(job_id, result)


if __name__ == '__main__':
    # get_talent_pool_size(18981)
    # do_batch()
    import_jobs()
