# -*- coding:utf-8 -*-
# @Time   : 2021/9/9
# @Author : quanbin


from yaml import load, Loader
import os

ROOT_DIR = os.path.dirname(__file__)

with open(os.path.join(ROOT_DIR, 'conf.yaml')) as stream:
    CONFIG = load(stream, Loader=Loader)