# -*- coding:utf-8 -*-
# @Time   : 2021/9/9
# @Author : quanbin


import plotly.graph_objects as go
import numpy as np
import time

from db_helper import *


def data_stastic():
    result = []
    fill_color = []
    ret_code = 0
    job_list = fetch_all_jobs()
    for job_id in job_list:
        data = get_job_last_10(job_id)
        if len(data) < 2:
            continue
        newest = data[0]
        last_10 = data[1:]
        last_10_avg = int(sum(last_10) / len(last_10))
        diff = last_10_avg * CONFIG['threshold']
        status = 'pass'
        if newest < last_10_avg - diff or newest > last_10_avg + diff:
            status = 'fail'
            ret_code = 1
        print("========", job_id, status, newest, last_10_avg, last_10)
        result.append([job_id, status, newest, last_10_avg, last_10])
        fill_color.append('lightgreen' if status == 'pass' else 'pink')

    return result, fill_color, ret_code


def gen_html_report(result, fill_color):
    fig = go.Figure(data=[go.Table(
        header=dict(values=['job_id', 'status', 'newest', 'last_10_avg', 'last_10'],
                    line_color='darkslategray',
                    fill_color='lightskyblue',
                    align='left'),
        cells=dict(values=np.transpose(result),  # 2nd column
                   line_color='darkslategray',
                   fill_color=[fill_color],
                   align='left'))
    ])
    current_time = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime(time.time()))
    fig['layout'].update(title="Talent Pool Size Monitor -- {}(UTC)".format(current_time))
    # fig.show()
    fig.write_html('report.html')


if __name__ == '__main__':
    result, fill_color, _ = data_stastic()
    gen_html_report(result, fill_color)

